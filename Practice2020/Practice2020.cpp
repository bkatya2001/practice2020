﻿// Для компьютеров на Windows. IP-адрес указан напрямую (строка 67), для тестирования в другой сети необходимо его изменить.
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <iostream>
#include <winsock2.h>
#include <stdio.h>

#pragma comment(lib, "ws2_32") // Библиотека для сокетов

using namespace std;

int error = 0; // Код ошибки 
char arr[11] = "";

void StartServer() { // Функция-Сервер
    WSADATA wsaData; // Определяем переменную для настройки библиотеки
    error = WSAStartup(MAKEWORD(2, 2), &wsaData); // Настройка
    if (error != 0) cout << "Библиотека - Ошибка" << endl; // Проверка

    SOCKET serverSock; // Создание сокета
    serverSock = socket(
        AF_INET, // Для сетевого протокола IPv4
        SOCK_STREAM, // Потоковый сокет
        IPPROTO_TCP // ТСР (для потоковых сокетов)
    );
    if (serverSock == -1) cout << "Сокет - Ошибка" << endl; // Проверка
    
    struct sockaddr_in saddr; // Связываем с IP-адресом компьютера 
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(3000); // Порт
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    error = bind(serverSock, (SOCKADDR*)&saddr, sizeof(saddr));
    if (error == -1) cout << "Соединение с IP - Ошибка" << endl; // Проверка

    listen(serverSock, 1); // Прослушивание порта

    SOCKET clientSock; // Клиентский сокет
    struct sockaddr_in kaddr;
    int kaddr_size = sizeof(kaddr);
    clientSock = accept(serverSock, (sockaddr*)&kaddr, &kaddr_size); // Извлечение из очереди запроса
    if (clientSock == INVALID_SOCKET) cout << "Подключение клиента - Ошибка" << endl; // Проверка

    while (clientSock != INVALID_SOCKET) { // Отправка данных каждую секунду
        send(clientSock, arr, sizeof(arr), 0);
        Sleep(1000);
    }

    closesocket(serverSock); // Закрытие сокета
    WSACleanup(); // Выгружаем WinSock
}

void StartClient() { // Функция-Клиент
    WSADATA wsaData; // Определяем переменную для настройки библиотеки
    error = WSAStartup(MAKEWORD(2, 2), &wsaData); // Настройка
    if (error != 0) cout << "Библиотека - Ошибка" << endl; // Проверка

    SOCKET serverSock; // Серверный сокет
    serverSock = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (serverSock == -1) cout << "Сокет - Ошибка" << endl; // Проверка

    struct sockaddr_in  addr; // Привязка
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3000);

    const char* servIP = "192.168.1.72"; // Задаём IP-адрес сервера
    addr.sin_addr.s_addr = inet_addr(servIP); // Преобразуем из символьного типа

    connect(serverSock, (sockaddr*)&addr, sizeof(addr)); // Установка связи

    while (serverSock != INVALID_SOCKET) { // Чтение
        char data[11];
        recv(serverSock, data, 11, 0);
        cout << data << endl;
    }

    closesocket(serverSock); // Закрытие сокета
    WSACleanup(); // Выгружаем WinSock
}

int main(void)
{
    system("chcp 1251 > text"); // Поддержка русского языка
    cout << "Этот компьютер ... ?\n1) Сервер\n2) Клиент\nВведите нужное число" << endl;;
    int data;
    cin >> data;
    if (data == 1) {
        cout << "Введите не более 10 символов для передачи" << endl;
        cin.clear();
        cin.ignore();
        cin.getline(arr, 11);
        StartServer();
    }
    if (data == 2) StartClient();
    return 0;
}